<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.95 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# progress 0.3.12

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_progress/develop?logo=python)](
    https://gitlab.com/ae-group/ae_progress)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_progress/release0.3.11?logo=python)](
    https://gitlab.com/ae-group/ae_progress/-/tree/release0.3.11)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_progress)](
    https://pypi.org/project/ae-progress/#history)

>ae_progress module 0.3.12.

[![Coverage](https://ae-group.gitlab.io/ae_progress/coverage.svg)](
    https://ae-group.gitlab.io/ae_progress/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_progress/mypy.svg)](
    https://ae-group.gitlab.io/ae_progress/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_progress/pylint.svg)](
    https://ae-group.gitlab.io/ae_progress/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_progress)](
    https://gitlab.com/ae-group/ae_progress/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_progress)](
    https://gitlab.com/ae-group/ae_progress/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_progress)](
    https://gitlab.com/ae-group/ae_progress/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_progress)](
    https://pypi.org/project/ae-progress/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_progress)](
    https://gitlab.com/ae-group/ae_progress/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_progress)](
    https://libraries.io/pypi/ae-progress)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_progress)](
    https://pypi.org/project/ae-progress/#files)


## installation


execute the following command to install the
ae.progress module
in the currently active virtual environment:
 
```shell script
pip install ae-progress
```

if you want to contribute to this portion then first fork
[the ae_progress repository at GitLab](
https://gitlab.com/ae-group/ae_progress "ae.progress code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_progress):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_progress/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.progress.html
"ae_progress documentation").
